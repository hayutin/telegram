import datetime
import logging
import mysql.connector
from mysql.connector import errorcode


# sq.create_table(cursor, CREATE_TABLE_BTC)
# sq.create_table(cursor, CREATE_TABLE_AVERAGE_BTC)

CREATE_DB = """
            CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'
            """

# CREATE_TABLE_BTC = """
#                    CREATE TABLE btc_price (
#                     price REAL NOT NULL,
#                     date timestamp);
#                    """
# CREATE_TABLE_AVERAGE_BTC = """
#                    CREATE TABLE btc_average (
#                     price REAL NOT NULL,
#                     date timestamp);
#                    """
# INSERT_ROW_BTC = """
#                  INSERT INTO 'btc_price'
#                  ('price', 'date')
#                  VALUES (%s, %s);
#                  """
# INSERT_ROW_AVERAGE_BTC = """
#                  INSERT INTO 'btc_average'
#                  ('price', 'date')
#                  VALUES (?, ?);
#                  """

class mariadb_db:
    def __init__(self, host, user, passwd, database):
        self.host = host
        self.user = user
        self.passwd = passwd
        self.databasename = database

        self.cnx = mysql.connector.connect(host=self.host, user=self.user, password=self.passwd)
        self.cursor = self.cnx.cursor()

    def create_database(self):
        try:
            self.cursor.execute(
                "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(self.databasename))
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

    def test_connect(self):
        try:
            self.cursor.execute("USE {}".format(self.databasename))
        except mysql.connector.Error as err:
            print("Database {} does not exists.".format(self.databasename))
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self.create_database()
                print("Database {} created successfully.".format(self.databasename))
                self.cursor.database = self.databasename
            else:
                print(err)
                exit(1)

    def db_connect(self,
                   host,
                   user,
                   passwd,
                   database):

        con = mysql.connector.connect(host=host,
                                      user=user,
                                      passwd=passwd,
                                      database=database)

        return con

    def get_db_cursor(self, con):
        return con.cursor()


    def create_table(self, cursor, format):
        try:
            cursor.execute(format)
        except mysql.connector.DatabaseError as err:
            logging.debug("table already exists")
            logging.debug(err)

    def insert(self, con, cursor, price, format):
        data_tuple = (price, datetime.datetime.now())
        cursor.execute(format, data_tuple)
        con.commit()
        logging.debug("price entered successfully \n")

    def query_all(self, cursor):
        q = """SELECT price, date from btc_price"""
        cursor.execute(q)
        records = cursor.fetchall()
        return records

    def query_average(self, cursor):
        q = """SELECT avg(price) from btc_price"""
        cursor.execute(q)
        records = cursor.fetchall()[0][0]
        return records

    def remove_old_data(self, con, cursor, days_old):
        check = cursor.execute("SELECT Count(*) FROM btc_price")
        rows = cursor.fetchall()[0][0]

        if rows > 1:
            q = "DELETE from btc_price where "\
                "date < datetime('now', '-{} days')".format(str(days_old))
            cursor.execute(q)
            con.commit()
            logging.debug("removed data older than: {} days".format(days_old))

