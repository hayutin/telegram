import http.server
import os
import socketserver
import time

directory = os.path.dirname(os.path.abspath(__file__))
PORT = 8000
DIRECTORY = directory + "/logs"


class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

class StartUp():
    def start(self):
        with socketserver.TCPServer(("", PORT), Handler) as httpd:
            print("serving at port", PORT)
            try:
                httpd.serve_forever()
            except KeyboardInterrupt:
                httpd.server_close()
                socketserver.close()

def main():
    http = StartUp()
    http.start()


if __name__ == '__main__':
    main()

