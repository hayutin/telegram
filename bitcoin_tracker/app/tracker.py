#!/usr/bin/python3

import logging
import logging.config
import os
import requests
import sys
import time
import yaml

#from sqlite_db import sqlite_db

directory = os.path.dirname(os.path.abspath(__file__))
# set up log files
# fix log path
try:
    os.mkdir(directory + "/logs/")
except FileExistsError:
    pass

f = open(directory + "/logs/logfile.log", "w")
f.write("Start Logs\n")
f.close()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s',
                              '%m-%d-%Y %H:%M:%S')

#stdout_handler = logging.StreamHandler(sys.stdout)
#stdout_handler.setLevel(logging.DEBUG)
#stdout_handler.setFormatter(formatter)

file_handler = logging.FileHandler(directory + '/logs/logfile.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


CREATE_TABLE_BTC = """
                   CREATE TABLE btc_price (
                    price REAL NOT NULL,
                    date timestamp);
                   """
CREATE_TABLE_AVERAGE_BTC = """
                   CREATE TABLE btc_average (
                    price REAL NOT NULL,
                    date timestamp);
                   """
INSERT_ROW_BTC = """
                 INSERT INTO btc_price
                 (price, date)
                 VALUES (%s, %s)
                 """
INSERT_ROW_AVERAGE_BTC = """
                 INSERT INTO btc_average
                 (price, date)
                 VALUES (%s, %s);
                 """

class BitCoinTracker():

    def __init__(self,
                 api_key,
                 bot_token,
                 chat_id,
                 time_interval=1,
                 threshold=32000):
            self.api_key = api_key
            self.bot_token = bot_token
            self.chat_id = chat_id
            self.time_interval = time_interval
            self.threshold = threshold


    def get_btc_price(self):
        url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
        headers = {
            'Accepts': 'application/json',
            'X-CMC_PRO_API_KEY': self.api_key
        }

        # make a request
        response = requests.get(url, headers=headers)
        response_json = response.json()

        # extract the bitcoin price
        btc_price = response_json['data'][0]
        return btc_price['quote']['USD']['price']

    def send_message(self, chat_id, msg):
        baseurl = "https://api.telegram.org/bot"

        url = "{}{}/sendMessage?chat_id={}&text={}".format(baseurl,
                                                          self.bot_token,
                                                          chat_id,
                                                          msg)
        # send the msg
        requests.get(url)
        logger.info("sent msg: {}".format(msg))

    def get_percentage_change(self, current_price, last_average):
        if 0 not in (current_price, last_average):
            if last_average is None:
                last_average = current_price
            change = float(current_price - last_average) / last_average * 100
        else:
            return 0
        return round(change, 4)

    def run(self, sq, con, cursor):

        sq.create_table(cursor, CREATE_TABLE_BTC)
        sq.create_table(cursor, CREATE_TABLE_AVERAGE_BTC)

        # get average
        average = sq.query_average(cursor)

        # get the current price
        price = self.get_btc_price()
        # add latest entry to db
        sq.insert(con, cursor, price, INSERT_ROW_BTC)

        percent_change = self.get_percentage_change(price, average)
        logger.info("precentage change: {}".format(percent_change))

        if price < self.threshold:
            #send_message(chat_id=chat_id, msg=f'BTC Price Drop Alert: {price}')
            self.send_message(chat_id=self.chat_id,
                            msg="BTC Price Drop Alert: {}".format(price))
            logger.info("sent PRICE_DROP message to telegram: {}".format(price))

        if abs(percent_change) > 1:
            txt = "BTC Percentage Change Alert:"
            msg =  "{} {}, Price: {}".format(txt,
                                            percent_change,
                                            price)
            self.send_message(chat_id=self.chat_id, msg=msg)
            logger.info(msg)

        if price > self.threshold and abs(percent_change) < 1:
            txt = "BTC, nothing interesting to report:"
            msg = txt
            self.send_message(chat_id=self.chat_id, msg=msg)
            logger.info(msg)

        # update latest average price
        if not average:
            average = price
            sq.insert(con, cursor, average, INSERT_ROW_AVERAGE_BTC)
            sq.remove_old_data(con, cursor, 30)

