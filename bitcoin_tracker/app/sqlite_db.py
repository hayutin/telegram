#!/usr/bin/python3

import datetime
import logging
import os
import sqlite3

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')


class sqlite_db():

    def db_connect(self, db_path=DEFAULT_PATH):
        con = sqlite3.connect(db_path,
                            detect_types=sqlite3.PARSE_DECLTYPES |
                            sqlite3.PARSE_COLNAMES)

        logging.debug("Connected to sqlite3")
        return con

    def get_db_cursor(self, con):
        return con.cursor()

    def create_table(self, cursor, format):
        try:
            cursor.execute(format)
        except sqlite3.OperationalError:
            logging.debug("table already exists")

    def insert(self, con, cursor, price, format):
        data_tuple = (price, datetime.datetime.now())
        cursor.execute(format, data_tuple)
        con.commit()
        logging.debug("price entered successfully \n")

    def query_all(self, cursor):
        q = """SELECT price, date from btc_price"""
        cursor.execute(q)
        records = cursor.fetchall()
        return records

    def query_average(self, cursor):
        q = """SELECT avg(price) from btc_price"""
        cursor.execute(q)
        records = cursor.fetchall()[0][0]
        return records

    def remove_old_data(self, con, cursor, days_old):
        check = cursor.execute("SELECT Count(*) FROM btc_price")
        rows = cursor.fetchall()[0][0]

        if rows > 1:
            q = "DELETE from btc_price where "\
                "date < datetime('now', '-{} days')".format(str(days_old))
            cursor.execute(q)
            con.commit()
            logging.debug("removed data older than: {} days".format(days_old))
