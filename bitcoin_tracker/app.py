#!/usr/bin/python3

import logging
import os
import time
import yaml

from app.tracker import BitCoinTracker
from app.simple_http_log import StartUp
from app.sqlite_db import sqlite_db
from app.mariadb_db import mariadb_db
from threading import Thread, current_thread

def http_server():
    http = StartUp()
    http.start()

def track():
    db_host = os.getenv('MYSQL_SERVICE_HOST')
    mysql_user = os.getenv('MYSQL_USER')
    mysql_passwd = os.getenv('MYSQL_PASSWORD')
    mysql_db = os.getenv('MYSQL_DATABASE')
    api_key = os.getenv('TRACKER_API_KEY') # pro.coinmarketcap.com
    bot_token = os.getenv('TRACKER_TELEGRAM_KEY') # telegram key
    chat_id = os.getenv('TRACKER_CHAT_ID') # telegram chat id
    time_interval = 1 * 10 # in seconds
    threshold = 46000 

    bct = BitCoinTracker(api_key,bot_token, chat_id, time_interval, threshold)
    # sq = sqlite_db()
    sq = mariadb_db(host=db_host,
                    user=mysql_user,
                    passwd=mysql_passwd,
                    database=mysql_db)
    sq.test_connect()
    con = sq.cnx
    cursor = sq.cursor
    while True:
        bct.run(sq, con, cursor)
        time.sleep(600)

def main():
    print("start http server")
    thread1 = Thread(target=http_server)
    thread1.setDaemon(False)
    thread1.start()

    print("start tracker")
    thread2 = Thread(target=track)
    thread2.setDaemon(False)
    thread2.start()
    track()

if __name__ == "__main__":
    main()
